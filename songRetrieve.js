/**
 * Created by thomas on 5/22/16.
 *
 *
 * Run this individually to add songs to the database.
 * storeMusicFiles() is the main function
 */
var fs = require('fs');
var mm = require('musicmetadata');
var mysql = require('mysql');

var currentSongArr = [];

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'test'
});
connection.connect();

//Make sure file ends with a slash (/)
function storeMusicFiles(folder) {
    var songs = fs.readdirSync(folder);
    if(songs == null) {}
    else
    {
        console.log(songs);
        console.log('\n\n\n');
        for (var i in songs) {
            if (songs[i].indexOf('.mp3') > -1 || songs[i].indexOf('.m4a') > -1) {
                var currentSong = folder + '/' + songs[i];
                currentSongArr.push(currentSong);
                insertValues(currentSong);
            }
            else {
                if(songs[i] != '.DS_Store') storeMusicFiles(folder + songs[i]);
            }
        }
    }
}

function insertValues (songLoc) {
    mm(fs.createReadStream(songLoc)
        , {duration: true}, function (err, metadata) {
            if (err) throw err;

            metadata.duration = secondsToTime(Math.round(metadata.duration));
            console.log('Title: ' + metadata.title);
            console.log('Artist: ' + metadata.artist[0]);
            console.log('Album: ' + metadata.album);
            console.log('Track: ' + metadata.track.no);
            console.log('Duration: ' + metadata.duration);
            //console.log(metadata);
            console.log('\n\n\n');
            SQLinsert(metadata.title, metadata.artist[0], metadata.album, metadata.track.no, metadata.duration, songLoc);
        });
}

function secondsToTime(secs) {
    var date = new Date(null);
    date.setSeconds(secs); // specify value for SECONDS here
    return date.toISOString().substr(14, 5);
}

function SQLinsert(stitle, sartist, salbum, strack, sduration, slocation) {
    var sqlValues = {
        Title: stitle, Artist: sartist, Album: salbum, Track: strack,
        Duration: sduration, Location: slocation
    };
    connection.query('INSERT INTO Songs SET ?', sqlValues, function (err) {
        //connection.release();
        if (!err)
            console.log('Successfully inserted values');
        else
            console.log('Error while performing Query. ' + err);
    });
}

storeMusicFiles('/Users/thomas/Music/iTunes/iTunes Media/Music/Foster The People/Torches/');