/**
 * Created by thomas on 5/21/16.
 */

//global variables
var currentSongNum = 1;
var totalSongsArr = [];
var songsArr = [];
var alreadyPlaying = false;
var angularData;
var angularDataTotal;
var playlistsArr = [];
var currentRoll;
var displayPlaylistsBelow = true;
var playedFromSearch = false;

$(window).load(function() {
    getPlaylists();
    $('#playlistToAddTo').html('Options');
});

//Closes popup windows if they are open and a click takes place outside them
$(document).click(function(event) {
    //If the a song menu is open and the click is not inside the menu, close it
    if(!$(event.target).closest('#songMenu').length) {
        if (!$(event.target).attr('class')) {
            if ($('#songMenu').is(":visible")) {
                //$('#songMenu').hide();
                $('#songMenu').fadeOut('fast');
            }
        }
        else if ($(event.target).attr('class').indexOf('glyphicon-option-horizontal') == -1) {
            if ($('#songMenu').is(":visible")) {
                //$('#songMenu').hide();
                $('#songMenu').fadeOut('fast');
            }
        }
    }
    //If the search table is open and the click is outside it, close it
    if(!$(event.target).closest('#songSearchBar').length) {
        if ($('#searchResultsTable').is(":visible")) {
            //$('#songMenu').hide();
            $('#searchResultsTable').fadeOut('fast');
        }
    }
});

//When you double click a song in the search dropdown, this function scrolls to it on the main table
$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 78
        }, 1000);
    });
};

function getPlaylists() {
    $.post("http://thomasmusic.ddns.net:8081/playlists", {}, function(data){
        var addSongPlaylistHtml = '';
        var selectPlaylistHtml = '';
        data = JSON.parse(data);
        for (var i in data) {
            playlistsArr[i] = data[i].Playlist.toString();
            if(data[i].Playlist.toString() != 'Songs') {
                addSongPlaylistHtml += '<a href="#" onclick="addSongToPlaylist(\'' + data[i].Playlist.toString() + '\')">' + data[i].Playlist.toString() + '</a>';
            }
            selectPlaylistHtml += '<a href="#" onclick="getSongs(\'' + data[i].Playlist.toString() + '\')">' + data[i].Playlist.toString() + '</a>';
        }
        selectPlaylistHtml += '<a href="#" class="addNewPlaylist" onclick="addPlaylistPopupFunc()">Add A New Playlist</a>';
        $('#addSongPlaylistsUl').html(addSongPlaylistHtml);
        $('#selectPlaylistsUl').html(selectPlaylistHtml);
    });
}

function addPlaylistPopupFunc() {
    $('#addPlaylistPopup').modal('show');
}

function addPlaylist() {
    var playlistName = $('#playlistName').val();
    $.post("http://thomasmusic.ddns.net:8081/createPlaylist", {Playlist: playlistName}, function(data){
        getPlaylists();
    });
}

function addSongToPlaylist(playlistName) {
    var songName = currentRoll.Title;
    var artistName = currentRoll.Artist;
    $.post("http://thomasmusic.ddns.net:8081/addSongToPlaylist", {Playlist: playlistName, Song: songName, Artist: artistName}, function(data){
        console.log(songName + ' by ' + artistName + ' was added to ' + playlistName);
    });
}

function displaySearchResultsTable() {
    $('#searchResultsTable').css('display', 'block');
}

function hideSearchResultsTable() {
    $('#searchResultsTable').css('display', 'none');
}

function placePlaylists() {
    if(!displayPlaylistsBelow) {
        var pixelsTaken = (playlistsArr.length-1) * -40 + 10;
        var playlistsWidth = $('#songMenu').width() - 10;
        $('#addSongPlaylistsUl').css('margin-left', playlistsWidth + 'px');
        $('#addSongPlaylistsUl').css('margin-top', pixelsTaken + 'px');
    }
    else {
        $('#addSongPlaylistsUl').css('margin-left', '-10px');
        $('#addSongPlaylistsUl').css('margin-top', '10px');
    }

}

$('#selectPlaylistsDiv').hover(function() {
    alert('hi');
});

angular.module('sortApp', []).controller('mainController', function($scope) {
    $scope.sortType     = 'artist'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order
    $scope.searchFish   = '';     // set the default search/filter term

    $scope.sushi = [
        //{ Title: 'asdf', Artist: 'asdf', Album: 'asdf', Track: 'asdf', Duration: 'asdf'}
    ];
    $scope.sushiTotal = [];

    $scope.changeAngularTotal = function() {
        $scope.sushiTotal = [];
        for(var i = 0; i < angularDataTotal.length; i++)
        {
            $scope.sushiTotal.push(angularDataTotal[i]);
            $scope.$apply();
        }
    };

    $scope.changeAngular = function() {
        $scope.sushi = [];
        for(var i = 0; i < angularData.length; i++)
        {
            $scope.sushi.push(angularData[i]);
            $scope.$apply();
        }
    };

    $scope.loadSound = function(songNum) {
        $('#searchResultsTable').css('display', 'none');
        loadSound($scope.sushi.indexOf(songNum));
    };

    $scope.loadSoundSearchBar = function(songNum) {
        $('#searchResultsTable').css('display', 'none');
        loadSoundSearchBar($scope.sushiTotal.indexOf(songNum));
    };

    $scope.setSearchFilter = function(artist) {
        $scope.searchFish = artist;
    };

    $scope.jumpToSong = function() {
        var p1 = new Promise(function(resolve, reject) {
            $scope.searchFish = '';
            resolve("Success!");
        });

        p1.then(function(value) {
            var songToJumpTo = $('td:contains(' + currentRoll.Title + ')');
            songToJumpTo.scrollView();
        }, function(reason) {
        });
    };

    $scope.doubleClickJumpToSong = function(roll) {
        $('#searchResultsTable').css('display', 'none');
        currentRoll = roll;
        var p1 = new Promise(function(resolve, reject) {
            $scope.searchFish = '';
            resolve("Success!");
        });

        p1.then(function(value) {
            var songToJumpTo = $('td:contains(' + currentRoll.Title + ')');
            songToJumpTo.scrollView();
        }, function(reason) {
        });
    };

    $scope.songOptions = function(song, element) {
        currentRoll = song;
        var w = window.innerHeight + window.pageYOffset;
        var y = element.pageY;
        var popupHeight = $('#songMenu').height()*2;
        if(y+popupHeight > w) {
            y = w - popupHeight;
            displayPlaylistsBelow = false;
        }
        else if(y+popupHeight+((playlistsArr.length-1)*45) > w) {
            displayPlaylistsBelow = false;
        }
        else displayPlaylistsBelow = true;

        $('#songMenu').css('left', '45px');
        $('#songMenu').css('top', y + 'px');
        $('#songMenu').css('display', 'block');
        $('#songMenuWords').html('<span class="wordsSpan">' + song.Artist + ' - ' + song.Title + '</span>');
    };
});

function callBackend() {
    $.post("http://thomasmusic.ddns.net:8081/getSongs", {Playlist: 'Songs'}, function(data){
        data = JSON.parse(data);
        angularDataTotal = data;
        for (var i in data) {
            songsArr[i] = [data[i].Title.toString(), data[i].Artist.toString()];
            totalSongsArr[i] = [data[i].Title.toString(), data[i].Artist.toString()];
        }
        angular.element(document.getElementById('songTable')).scope().changeAngularTotal();
        getSongs('Songs');
    });
}

function getSongs(playlist) {
    if(playlist == 'Songs') {
        angularData = angularDataTotal;
        for (var i in angularDataTotal) {
            songsArr[i] = [angularDataTotal[i].Title.toString(), angularDataTotal[i].Artist.toString()];
        }
        angular.element(document.getElementById('songTable')).scope().changeAngular();
    }
    else {
        $.post("http://thomasmusic.ddns.net:8081/getSongs", {Playlist: playlist}, function(data){
            data = JSON.parse(data);
            angularData = data;
            for (var i in data) {
                songsArr[i] = [data[i].Title.toString(), data[i].Artist.toString()];
            }
            angular.element(document.getElementById('songTable')).scope().changeAngular();
        });
    }
}

var source;
var initialStart = true;
var responseData;
function process(Data) {
    alreadyPlaying = true;
    source = context.createBufferSource(); // Create Sound Source
    context.decodeAudioData(Data, function (buffer) {
        source.buffer = buffer;
        source.connect(context.destination);
        source.start(context.currentTime);
        console.log(context.sampleRate);


        source.onended = function() {
            console.log('Your audio has finished playing');
            //alreadyPlaying = false;
            if(playedFromSearch) {
                loadSoundSearchBar(++currentSongNum)
            }
            else loadSound(++currentSongNum);
        }
    });
}

function loadSoundSearchBar(songNum) {
    playedFromSearch = true;
    var title = totalSongsArr[songNum][0];
    var artist = totalSongsArr[songNum][1];
    loadSoundMain(songNum, title, artist);
}

function loadSound(songNum) {
    playedFromSearch = false;
    var title = songsArr[songNum][0];
    var artist = songsArr[songNum][1];
    loadSoundMain(songNum, title, artist);
}

function loadSoundMain(songNum, title, artist) {
    $('#songInfoDisplay').html(title + " - " + artist);
    currentSongNum = songNum;

    if (alreadyPlaying) {
        source.stop();
        context.close();
        alreadyPlaying = false;
    }
    if(initialStart) initialStart = false;

    window.AudioContext = window.AudioContext||window.webkitAudioContext || false;
    context = new AudioContext();
    context.sampleRate = 1000;

    if(!context) alert("Sorry, but the Web Audio API is not supported by your browser.");
    
    var request = new XMLHttpRequest();
    request.open("POST", "http://thomasmusic.ddns.net:8081/playSong", true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.responseType = "arraybuffer";

    request.onload = function() {
        responseData = request.response;
        process(responseData);
        document.title = 'Thomas Music - ' + title + ' by ' + artist;
        $('#playpause').find(".glyphicon").removeClass("glyphicon-play").addClass("glyphicon-pause");
    };
    request.send(JSON.stringify({Title: title, Artist: artist}));

}

//play/pause button - starts and stops
function stopMusic() {
    if(alreadyPlaying) {
        $('#playpause').find(".glyphicon").removeClass("glyphicon-pause").addClass("glyphicon-play");
        context.suspend();
        //source.stop(); this works
        alreadyPlaying = false;
    }
    else if (initialStart) {
        $('#playpause').find(".glyphicon").removeClass("glyphicon-play").addClass("glyphicon-pause");
        loadSound(currentSongNum);
    }
    else {
        $('#playpause').find(".glyphicon").removeClass("glyphicon-play").addClass("glyphicon-pause");
        context.resume();
        alreadyPlaying = true;
    }
}

function backOneSong() {
    if(!alreadyPlaying) $('#playpause').find(".glyphicon").removeClass("glyphicon-play").addClass("glyphicon-pause");
    // loadSound(--currentSongNum);
    if(playedFromSearch) {
        loadSoundSearchBar(--currentSongNum)
    }
    else loadSound(--currentSongNum);
}

function forwardOneSong() {
    if(!alreadyPlaying) $('#playpause').find(".glyphicon").removeClass("glyphicon-play").addClass("glyphicon-pause");
    // loadSound(++currentSongNum);
    if(playedFromSearch) {
        loadSoundSearchBar(++currentSongNum)
    }
    else loadSound(++currentSongNum);
}