# Host your own music server with the creatively named **Music Website**. #

Music Website can access any song in your iTunes library and stream it to a web browser. Music Website is currently functioning and stable, allowing you to access all of your songs, make custom playlists, and quickly search for songs in your library. 

You will need to set up a web server from your machine, as well as tweak some code in order to use this application for yourself (instructions on database setup, required downloads coming soon). For legal purposes this should only be run locally, however if you wish to host it on a public domain, it will work. A reasonable alternative would be to host locally and use a VPN to access your home network. 

#**v0.1**#
* Playlists - create/modify/delete playlists 
* Navigation bar with audio controls, song info, playlist dropdown
* Main area displays rows of songs with: song name, artist, album and song length, play button, and options button
* Quick Search bar - search your library by song, artist or album
* Options button allows you to add/remove song from playlist, listen next

#**v0.2** - coming soon#
* Instructions on setting up this project for yourself
* Accounts - Sign in to Music Website and personalize your experience. If more than one person is using your website, create accounts to have separate playlists/default homepage/etc.
* Album art - display album art alongside music data