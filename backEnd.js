/**
 * Created by thomas on 5/21/16.
 */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var nodeSQL = require('./nodeSQL');
var fs = require('fs');
var ffmpeg = require('fluent-ffmpeg');
var child_process = require('child_process');
//var chunkingStreams = require('chunking-streams');
//var SizeChunker = chunkingStreams.SizeChunker;

//var ms = require('mediaserver');
// var filePath = '/Users/thomas/Music/iTunes/iTunes Media/Music/Drake/Views/17 Summers Over Interlude.mp3';
var filePath = '';

var bodyParser = require("body-parser");
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/helpers",  express.static(__dirname + '/helpers'));

app.get('/', function(req, res){
    res.sendfile('coolWebpage.html');
});


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    //res.setHeader('Access-Control-Allow-Origin', 'http://thomaswebsite.com');
    //res.setHeader('Access-Control-Allow-Origin', 'http://10.0.1.12');
    res.setHeader('Access-Control-Allow-Origin', 'http://thomasmusic.ddns.net');
    // res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    //res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



app.post('/addSongToPlaylist', function(req, res) {
    var pname = req.body.Playlist.replace(/ /g, '_');
    var stitle = req.body.Song;
    var artist = req.body.Artist;
    nodeSQL.SQLaddSongToPlaylist(pname, stitle, artist, function(err, rows){
        res.end(JSON.stringify(rows));
    });
});

app.post('/createPlaylist', function(req, res) {
    var pname = req.body.Playlist.replace(/ /g, '_');
    nodeSQL.SQLcreatePlaylist(pname, function(err, rows){
        res.end(JSON.stringify(rows));
    });
});

app.post('/playlists', function(req, res) {
    console.log('Playlists:');
    var result = [];
    nodeSQL.SQLgetPlaylists(function(err, rows){
        for (var i = 1; i < rows.length; i++) {
            result.push({"Playlist": rows[i].TABLE_NAME.replace(/_/g, ' ')});
            console.log('  ' + rows[i].TABLE_NAME.replace(/_/g, ' '));
        }
        res.end(JSON.stringify(result));
    });
});

app.post('/getSongs', function (req, res) {
    var playlist = req.body.Playlist.replace(/ /g, '_');
    var result = [];
    nodeSQL.SQLquerySongs(playlist, function(err, rows) {
        if (err)
            console.log('Error while performing Query. ' + err);

        else {
            //Prints out all songs
            //console.log('The solution is: ', rows);
            for (var i = 0; i < rows.length; i++) {
                result.push({"Title": rows[i].Title, "Artist": rows[i].Artist
                    ,"Album": rows[i].Album, "Track": rows[i].Track, "Duration": rows[i].Duration});
            }
            res.end(JSON.stringify(result));
        }
    });
});

app.post('/playSong', function (req, res) {
    var title = req.body.Title;
    var artist = req.body.Artist;
    console.log('Current song: ' + title + ' - ' + artist);
    nodeSQL.SQLplaySong(title.replace(/'/g, "''"), artist, function(err, rows) {
        if (err)
            console.log('Error while performing Query. ' + err);

        else {
            filePath = rows[0].Location;
            res.set({'Content-Type': 'audio/mpeg'});
            filePath = filePath.replace(/ /g, '\ ');

            var songFileArr = filePath.split('/');
            var songFile = songFileArr[songFileArr.length - 1];
            var songFileLoc = '/Users/thomas/Desktop/96kbitSongs/' + songFile;

            try {
                // Do something
                fs.accessSync(songFileLoc, fs.F_OK);
                var readStream = fs.createReadStream(songFileLoc);
                readStream.pipe(res);

            } catch (e) {
                // It isn't accessible
                ffmpeg(filePath)
                    .addInput(filePath)
                    .outputOptions([
                        '-b:a 128k',
                        '-map a'
                    ])
                    .on('error', function(err, stdout, stderr) {
                        console.log("ffmpeg stdout:\n" + stdout);
                        console.log("ffmpeg stderr:\n" + stderr);
                    })
                    .on('end', function() {
                        var readStream = fs.createReadStream(songFileLoc);
                        readStream.pipe(res);
                    })
                    .output(songFileLoc)
                    .run();
            }
        }
    });
});


var server = http.listen(8081, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port);

});