//mysql documentation: https://github.com/felixge/node-mysql#escaping-query-values
//server location: /Users/thomas/WebstormProjects/NodeProj2
    //change /private/etc/apache2/httpd.conf DocumentRoot to fix
    //also granted permissions in <Directory>

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'test'
});

connection.connect();

function SQLquery(queryString, cb) {
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'test'
    });
    connection.query(queryString, function(err, rows) {
        if(err) return cb(err);
        cb(null, rows);
    });

}

module.exports.SQLquery = function(cb) {
    SQLquery('Select * from NodeUsers', cb)
};

module.exports.SQLquerySongs = function(playlist, cb) {
    SQLquery("Select * from " + playlist + " Order By Artist, Album, Track, Title", cb)
};

module.exports.SQLplaySong = function(title, artist, cb) {
    SQLquery("Select Location from Songs WHERE Title='" + title + "' AND Artist='" + artist + "'", cb)
};

module.exports.SQLgetPlaylists = function(cb) {
    SQLquery("Select TABLE_NAME from INFORMATION_SCHEMA.TABLES WHERE table_schema = 'test'", cb)
};

module.exports.SQLcreatePlaylist = function(playlistName, cb) {
    SQLquery("CREATE TABLE " + playlistName +
        " (Title varchar(100), " +
        "Artist varchar(100), " +
        "Album varchar(100), " +
        "Track int, " +
        "Duration varchar(5), " +
        "Location varchar(200) PRIMARY KEY);", cb);
};

module.exports.SQLaddSongToPlaylist = function(playlistName, title, artist, cb) {
    SQLquery("INSERT INTO " + playlistName + " SELECT * FROM Songs WHERE Title = '" + title + "' AND Artist = '" + artist + "'", cb)
};



connection.end();